package man.client.ice.signove.com.sh_central_mobile.services;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by Signove on 01/09/2016.
 */
public class HTTPClient {

    private static HTTPClient httpClient;

    private RequestQueue requestQueue;
    private Context context;

    private HTTPClient(Context context){
        if(requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
            //requestQueue.start();
        }
    }

    public static synchronized HTTPClient getInstance(Context context) throws IllegalStateException{
        if(context == null){
            throw new IllegalStateException(HTTPClient.class.getSimpleName() +
                    " context is null, call getInstance(...) and passing the context not null");
        }
        if(httpClient == null){
            httpClient = new HTTPClient(context);
        }
        return httpClient;
    }

    public static synchronized HTTPClient getInstance() throws IllegalStateException{
        if(httpClient == null){
            throw new IllegalStateException(HTTPClient.class.getSimpleName() +
                    " is not initialized, call getInstance(...) first");
        }
        return httpClient;
    }

    public void requestJson(int method, String url, JSONObject json, Response.Listener<JSONObject> success, Response.ErrorListener error){
        requestQueue.add(new JsonObjectRequest(method, url, json, success, error));
    }

}
