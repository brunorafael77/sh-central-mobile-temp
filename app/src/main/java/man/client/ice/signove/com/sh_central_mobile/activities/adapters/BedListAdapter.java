package man.client.ice.signove.com.sh_central_mobile.activities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import man.client.ice.signove.com.sh_central_mobile.R;
import man.client.ice.signove.com.sh_central_mobile.models.Bed;

/**
 * Created by Signove on 01/09/2016.
 */
public class BedListAdapter extends ArrayAdapter<Bed> {

    private LayoutInflater mInflater;

    public BedListAdapter(Context context, int resource, List<Bed> beds) {
        super(context, resource, beds);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(int position, View view, ViewGroup parent) {
        View rowView= this.mInflater.inflate(R.layout.template_bed_list, parent, false);

        TextView patientNameView = (TextView) rowView.findViewById(R.id.patient_name);
        TextView infoView = (TextView) rowView.findViewById(R.id.info);
        TextView bedIdView = (TextView) rowView.findViewById(R.id.bed_id);

        Bed bed = this.getItem(position);

        patientNameView.setText(bed.getPatient().getName());
        infoView.setText("Description : " + bed.getPatient().getName());
        bedIdView.setText(bed.getIdentification());

        return rowView;

    };

}
