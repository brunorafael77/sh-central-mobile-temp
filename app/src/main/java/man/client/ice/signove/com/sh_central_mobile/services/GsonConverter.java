package man.client.ice.signove.com.sh_central_mobile.services;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONObject;

/**
 * Created by Signove on 01/09/2016.
 */
public class GsonConverter{

    private static GsonConverter gc;

    Gson gson = new Gson();

    private GsonConverter(){

    }

    public GsonConverter getInstance(){
        if(gc == null){
            gc = new GsonConverter();
        }
        return gc;
    }

    public Object convertJsonToObject(String response, Class type){
        return gson.fromJson(response, type);
    }

    public Object convertJsonToObject(JSONObject response, Class type){
        JsonParser parser = new JsonParser();
        JsonElement mJson =  parser.parse(response.toString());
        return gson.fromJson(mJson, type);
    }

    public String convertObjectToString(Object response, Class type){
        return gson.toJson(response, type);
    }
}
