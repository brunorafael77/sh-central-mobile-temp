package man.client.ice.signove.com.sh_central_mobile.models;

/**
 * Created by Signove on 01/09/2016.
 */
public class Bed {

    private String identification;
    private Patient patient;

    public Bed(String identification, Patient patient){
        this.identification = identification;
        this.patient = patient;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String id) {
        this.identification = id;
    }

    public Patient getPatient() {
        return patient;
    }
    public void setPatient(Patient p) {
        this.patient = p;
    }

}
