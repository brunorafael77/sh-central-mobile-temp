package man.client.ice.signove.com.sh_central_mobile.models;

/**
 * Created by Signove on 01/09/2016.
 */
public class Patient {
    private String name;

    public Patient(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
