package man.client.ice.signove.com.sh_central_mobile.activities.main.fragments;

import android.os.Bundle;
import android.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import man.client.ice.signove.com.sh_central_mobile.R;
import man.client.ice.signove.com.sh_central_mobile.activities.adapters.BedListAdapter;
import man.client.ice.signove.com.sh_central_mobile.constants.URL;
import man.client.ice.signove.com.sh_central_mobile.models.Bed;
import man.client.ice.signove.com.sh_central_mobile.models.Patient;
import man.client.ice.signove.com.sh_central_mobile.services.HTTPClient;

/**
 * Created by Signove on 30/08/2016.
 */
public class BedListPageFragment extends ListFragment {

    private HTTPClient httpClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.httpClient = HTTPClient.getInstance(getActivity().getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance){
        final View view =  inflater.inflate(R.layout.fragment_main_page, container, false);

        //request bed for server
        List<Bed> list = new ArrayList<>();

        JSONObject params = new JSONObject();
        try {
            params.put("id", "1");
            this.httpClient.requestJson(Request.Method.GET, URL.GET_ALL_BEDS_FROM_ROOM, params,
                    callbackSuccess(view), callbackError(view) );
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;

    }

    private Response.Listener<JSONObject> callbackSuccess(final View view){

        final List<Bed> list = new ArrayList<>();
        list.add(new Bed("1", new Patient("Bruno Rafael")));
        list.add(new Bed("2", new Patient("Daniel da Silva")));
        list.add(new Bed("3", new Patient("Alberto de Fonseca")));
        list.add(new Bed("4", new Patient("Carla Rafaela")));
        list.add(new Bed("5", new Patient("Gabriel Nunes")));
        list.add(new Bed("6", new Patient("Junior Leitão")));
        list.add(new Bed("7", new Patient("Lafael de Albuquerque")));
        list.add(new Bed("8", new Patient("Leandro Leonardo")));
        list.add(new Bed("9", new Patient("Debisson Pereira")));

       return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response.toString());
                ArrayAdapter<Bed> adapter = new BedListAdapter(getActivity().getApplicationContext(),
                        android.R.layout.simple_list_item_1, list);

                ListView lv = (ListView) view.findViewById(android.R.id.list);

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        Toast.makeText(getActivity().getApplicationContext(),
                                "Click ListItem Number " + position, Toast.LENGTH_LONG)
                                .show();
                    }
                });

                setListAdapter(adapter);
            }
        };
    }

    private Response.ErrorListener callbackError(final View view){
        final List<Bed> list = new ArrayList<>();
        list.add(new Bed("1", new Patient("Bruno Rafael")));
        list.add(new Bed("2", new Patient("Daniel da Silva")));
        list.add(new Bed("3", new Patient("Alberto de Fonseca")));
        list.add(new Bed("4", new Patient("Carla Rafaela")));
        list.add(new Bed("5", new Patient("Gabriel Nunes")));
        list.add(new Bed("6", new Patient("Junior Leitão")));
        list.add(new Bed("7", new Patient("Lafael de Albuquerque")));
        list.add(new Bed("8", new Patient("Leandro Leonardo")));
        list.add(new Bed("9", new Patient("Debisson Pereira")));
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ArrayAdapter<Bed> adapter = new BedListAdapter(getActivity().getApplicationContext(),
                        android.R.layout.simple_list_item_1, list);

                ListView lv = (ListView) view.findViewById(android.R.id.list);

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        Toast.makeText(getActivity().getApplicationContext(),
                                "Click ListItem Number " + position, Toast.LENGTH_LONG)
                                .show();
                    }
                });

                setListAdapter(adapter);
            }
        };
    }
}
