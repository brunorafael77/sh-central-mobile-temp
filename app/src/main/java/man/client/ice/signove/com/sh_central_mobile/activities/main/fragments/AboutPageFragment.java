package man.client.ice.signove.com.sh_central_mobile.activities.main.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import man.client.ice.signove.com.sh_central_mobile.R;

/**
 * Created by Signove on 30/08/2016.
 */
public class AboutPageFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance){
        return inflater.inflate(R.layout.fragment_about_page, container, false);

    }
}
