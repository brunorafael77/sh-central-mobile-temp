package man.client.ice.signove.com.sh_central_mobile.constants;

/**
 * Created by Signove on 01/09/2016.
 */
public class URL {
    public static final String SERVER = "http://192.168.101.119";
    public static final String CENTRAL_PORT_PROCESS = ":1337";

    public static final String GET_ALL_BEDS_FROM_ROOM = SERVER + CENTRAL_PORT_PROCESS + "/room";

}
